import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

export const AllCustomers = new Mongo.Collection('customers');

CustomerSchema = new SimpleSchema({
  _id: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    optional: true
  },

  customerId: {
    type: Number,
    optional: true
  },

  email: {
    type: String,
    optional: true
  },

  accepts_marketing: {
    type: Boolean,
    optional: true
  },

  created_at: {
    type: Date,
    optional: true
  },

  updated_at: {
    type: Date,
    optional: true
  },

  first_name: {
    type: String,
    optional: true
  },

  last_name: {
    type: String,
    optional: true
  },

  orders_count: {
    type: Number,
    optional: true
  },

  total_spent: {
    type: String,
    optional: true
  },

  last_order_id: {
    type: Number,
    optional: true
  },

  active: {
    type: Boolean,
    optional: true
  },

  last_order_name: {
    type: String,
    optional: true
  },

  address1: {
    type: String,
    optional: true
  },

  address2: {
    type: String,
    optional: true
  },

  city: {
    type: String,
    optional: true
  },

  province: {
    type: String,
    optional: true
  },

  province_code: {
    type: String,
    optional: true
  },

  country: {
    type: String,
    optional: true
  },

  zip: {
    type: String,
    optional: true
  },

  phone: {
    type: String,
    optional: true
  },

  systemCreatedOn: {
    type: Date,
    optional: true
  }

});

AllCustomers.attachSchema( CustomerSchema );