import { AllOrders } from '../imports/api/Orders/AllOrders/AllOrders';
import { AllAttributes } from '../imports/api/Orders/OrderAttributes/OrderAttributes';
import { AllLineItems } from '../imports/api/Orders/LineItems/LineItems';
import {AllCustomers} from "../imports/api/Customers/AllCustomers/AllCustomers";

Meteor.publish('publishAllOrders', function () {
  return AllOrders.find({});
});

Meteor.publish('publishAllAttributes', function () {
  return AllAttributes.find({});
});

Meteor.publish('publishAllLineItems', function () {
  return AllLineItems.find({});
});

Meteor.publish('singleOrder', function (thisOrderId) {
  return AllOrders.find({_id: thisOrderId});
});

Meteor.publish('singleAttributes', function (thisOrderId) {
  return AllAttributes.find({orderId: thisOrderId});
});

Meteor.publish('orderLineItems', function (thisOrderId) {
  return AllLineItems.find({orderId: thisOrderId});
});

Meteor.publish('publishAllCustomers', function () {
  return AllCustomers.find({});
});