import { HTTP } from 'meteor/http';
import { AllOrders } from '../imports/api/Orders/AllOrders/AllOrders';
import { AllAttributes } from '../imports/api/Orders/OrderAttributes/OrderAttributes';
import { AllLineItems } from '../imports/api/Orders/LineItems/LineItems';
import { AllCustomers } from "../imports/api/Customers/AllCustomers/AllCustomers";

Meteor.methods({

    /////All System User Methods/////
    ////////////////////////////////

  //add users
  addSystemUsers: function (getFirst,getLast,getEmail,getPhone,getPass,getAccount) {

    var addUser = Accounts.createUser({
      email:getEmail,
      password:getPass,
      profile: {
        firstName:getFirst,
        lastName:getLast,
        phone:getPhone,
        accountType:getAccount,
        createdAt: new Date()
      },
    });

    //add user to chosen role
    Roles.addUsersToRoles(addUser,getAccount);
  },

  /////All Shopify API Methods/////
  ////////////////////////////////

  ///Get Order Data
  getAllOrders: function () {

    /////begin api call//////
    let options = {
      auth: "3b62bea7c582b20bb5dd33d702f58d8e:313050108ae29bdf1eeee3500aa75c19",
      headers: {
        "authorization": "Basic M2I2MmJlYTdjNTgyYjIwYmI1ZGQzM2Q3MDJmNThkOGU6MzEzMDUwMTA4YWUyOWJkZjFlZWVlMzUwMGFhNzVjMTk=",
        "cache-control": "no-cache"
      }
    };
    let callResult;

    //get full order limit with status of any.
    if (AllOrders.find({}).count() > 0) {
      //pull all orders since the last order pulled.
      let lastOrder = AllOrders.findOne({}, {sort:{$natural:-1}}).realOrderId;
      callResult = HTTP.call('GET', 'https://mokaboka.myshopify.com/admin/orders.json?status=any&since_id=' + lastOrder + '&limit=250', options);
    } else {
      //if no documents exist in the collection pull all orders (limit 250)
      callResult = HTTP.call('GET', 'https://mokaboka.myshopify.com/admin/orders.json?status=any&limit=250', options);
    }
    //////end api call/////

    //define api results
    let orderData = callResult.data.orders;

    orderData.forEach((order)=>{

      //set function variables
      let getId = order.name;
      let realId = order.id;
      let getDate = order.created_at;
      let getBillingFirst = order.customer.first_name;
      let getBillingLast = order.customer.last_name;
      let getCustomerId = order.customer.id;
      let getEmail = order.customer.email;
      let AttrData = order.customer.note;


      //get Attribute data
      let noteAttrs = order.note_attributes;

      //remove # from string
      let cleanId = getId.replace('#','');

      //convert sting to number
      let numID = Number(cleanId);

      //check if id is already in DB
      let checkOrderDb = AllOrders.findOne({orderId:numID});

      if(checkOrderDb === undefined || null){

        let newOrder = AllOrders.insert({
          realOrderId: realId,
          orderId: numID,
          orderDate: getDate,
          hasAttr: false,
          customerId:getCustomerId,
          customerFirstName: getBillingFirst,
          customerLastName: getBillingLast,
          customerEmail: getEmail,
          printStatus: false,
          createdOn: new Date(),
        });


        ////process refunds
        let refund = order.refunds;

        if (refund.length > 0){

          AllOrders.update(newOrder, {
            $set: {
              refund: true
            }
          });

        }

        let checkAttDb = AllAttributes.findOne({orderId:newOrder});
        let currentOrder = newOrder;

        //add orders Attr data (if it is present)
        if (AttrData) {
          let getAttrData = AttrData.split("\n").reduce(function(obj, str) {
            let strParts = str.split(":");
            if (strParts[0] && strParts[1]) { //<-- Make sure the key & value are not undefined
              obj[strParts[0].replace(/\s+/g, '')] = strParts[1].trim(); //<-- Get rid of extra spaces at beginning of value strings
            }
            return obj;
          }, {});

          if (checkAttDb === undefined || null && AttrData) {
            let newAttr = AllAttributes.insert({
              orderId: currentOrder,
              createdAt: new Date(),
              child1FirstName: getAttrData.Child1FirstName,
              child1Gender: getAttrData.Child1Gender,
              child1HairColor: getAttrData.Child1HairColor,
              child1HairStyle: getAttrData.Child1HairfStyle,
              child1SkinTone: getAttrData.Child1SkinTone,
              child2FirstName: getAttrData.Child2FirstName,
              child2Gender: getAttrData.Child2Gender,
              child2HairColor: getAttrData.Child2HairColor,
              child2HairStyle: getAttrData.Child2HairStyle,
              child2SkinTone: getAttrData.Child2SkinTone,
              relationship1to2: getAttrData.Relationship1to2,
              relationship2to1: getAttrData.Relationship2to1
            });

            AllOrders.update(currentOrder,{
              $set: {
                hasAttr: true,
                AttrId: newAttr
              }
            });
          }
        }

        if (!AttrData && noteAttrs.length > 0 && checkAttDb === undefined || null) {

          let newAttr = AllAttributes.insert({
            orderId: currentOrder,
            createdAt: new Date()
          });


          noteAttrs.forEach((item)=>{
            //set noteAttrs vars
            let child1Name;
            let child1Gender;
            let child1Birth;
            let child1HairColor;
            let child1HairStyle;
            let child1SkinTone;
            let child1to2;
            let child1FavColor;
            let child2Name;
            let child2Gender;
            let child2Birth;
            let child2HairColor;
            let child2HairStyle;
            let child2SkinTone;
            let child2FavColor;
            let child2to1;
            let noteDedication;
            let noteImage;

            if (item.name === "Child 1 Name") {
              child1Name = item.value;

              AllAttributes.update(newAttr,{
                $set: {
                  child1FirstName: child1Name
                }
              });
            }

            if (item.name === "Child 1 Gender") {
              child1Gender = item.value;
              AllAttributes.update(newAttr,{
                $set: {
                  child1Gender: child1Gender
                }
              });
            }

            if (item.name === "Child 1 birthdate") {
              child1Birth = item.value;
              AllAttributes.update(newAttr,{
                $set: {
                  child1Birthday: child1Birth
                }
              });
            }

            if (item.name === "Child 1 Hair Color") {
              child1HairColor = item.value;
              AllAttributes.update(newAttr,{
                $set: {
                  child1HairColor: child1HairColor
                }
              });
            }

            if (item.name === "Child 1 Hair Style") {
              child1HairStyle = item.value;
              AllAttributes.update(newAttr,{
                $set: {
                  child1HairStyle: child1HairStyle
                }
              });
            }

            if (item.name === "Child 1 Skin Tone") {
              child1SkinTone = item.value;
              AllAttributes.update(newAttr,{
                $set: {
                  child1SkinTone: child1SkinTone
                }
              });
            }

            if (item.name === "Child 1 to 2") {
              child1to2 = item.value;
              AllAttributes.update(newAttr,{
                $set: {
                  relationship1to2: child1to2
                }
              });
            }

            if (item.name === "Child 1 Favorite color") {
              child1FavColor = item.value;
              AllAttributes.update(newAttr,{
                $set: {
                  child1FavoriteColor: child1FavColor
                }
              });
            }

            if (item.name === "Child 2 Name") {
              child2Name = item.value;
              AllAttributes.update(newAttr,{
                $set: {
                  child2FirstName: child2Name
                }
              });
            }

            if (item.name === "Child 2 Gender") {
              child2Gender = item.value;
              AllAttributes.update(newAttr,{
                $set: {
                  child2Gender: child2Gender
                }
              });
            }

            if (item.name === "Child 2 Birthdate") {
              child2Birth = item.value;
              AllAttributes.update(newAttr,{
                $set: {
                  child2Birthday: child2Birth
                }
              });
            }

            if (item.name === "Child 2 Hair Color") {
              child2HairColor = item.value;
              AllAttributes.update(newAttr,{
                $set: {
                  child2HairColor: child2HairColor
                }
              });
            }

            if (item.name === "Child 2 Hair Style") {
              child2HairStyle = item.value;
              AllAttributes.update(newAttr,{
                $set: {
                  child2HairStyle: child2HairStyle
                }
              });
            }

            if (item.name === "Child 2 Skin Tone") {
              child2SkinTone = item.value;
              AllAttributes.update(newAttr,{
                $set: {
                  child2SkinTone: child2SkinTone
                }
              });
            }

            if (item.name === "Child 2 Favorite color") {
              child2FavColor = item.value;
              AllAttributes.update(newAttr,{
                $set: {
                  child2FavoriteColor: child2FavColor
                }
              });
            }

            if (item.name === "Child 2 to 1") {
              child2to1 = item.value;
              AllAttributes.update(newAttr,{
                $set: {
                  relationship2to1: child2to1
                }
              });
            }

            if (item.name === "Dedication") {
              noteDedication = item.value;
              AllAttributes.update(newAttr,{
                $set: {
                  dedication: noteDedication
                }
              });
            }

            if (item.name === "Image") {
              noteImage = item.value;
              AllAttributes.update(newAttr,{
                $set: {
                  noteAtrImage: noteImage
                }
              });
            }
          });

          AllOrders.update(currentOrder,{
            $set: {
              hasAttr: true,
              AttrId: newAttr
            }
          });
        }

        let lineItems = order.line_items;

        if (lineItems.length > 0 ) {
          lineItems.forEach((book)=>{
            let bookProps = book.properties;

            let setLineItem = AllLineItems.insert({
              lineItemId: book.id,
              orderId: newOrder,
              title: book.title,
              quantity: book.quantity,
              price: book.price,
              variant_title: book.variant_title,
              product_id: book.product_id,
              name: book.name,
              recurring: false,
              createdOn: new Date()
            });

            if (bookProps.length > 0) {
              bookProps.forEach((prop)=>{
                if (prop.name === 'customImage') {
                  AllLineItems.update(setLineItem, {
                    $set: {
                      customImage: prop.value
                    }
                  });
                }

                if (prop.name === 'Recurring Order') {
                  AllLineItems.update(setLineItem, {
                    $set: {
                      recurring: true,
                      recurringTime: prop.value
                    }
                  });
                }
              });
            }
          });
        }
      }
    });
  },

  /////Retrieve all customer data
  getAllCustomers: function () {

    /////begin api call//////
    let options = {
      auth: "3b62bea7c582b20bb5dd33d702f58d8e:313050108ae29bdf1eeee3500aa75c19",
      headers: {
        "authorization": "Basic M2I2MmJlYTdjNTgyYjIwYmI1ZGQzM2Q3MDJmNThkOGU6MzEzMDUwMTA4YWUyOWJkZjFlZWVlMzUwMGFhNzVjMTk=",
        "cache-control": "no-cache"
      }
    };
    let callResult;

    //get full order limit with status of any.
    if (AllCustomers.find({}).count() > 0) {
      //pull all orders since the last order pulled.
      let lastCustomer = AllCustomers.findOne({}, {sort:{$natural:-1}}).customerId;
      callResult = HTTP.call('GET', 'https://mokaboka.myshopify.com/admin/customers.json?since_id=' + lastCustomer + '&limit=250', options);
    } else {
      //if no documents exist in the collection pull all orders (limit 250)
      callResult = HTTP.call('GET', 'https://mokaboka.myshopify.com/admin/customers.json?limit=250', options);
    }
    //////end api call/////

    //define api results
    let customerData = callResult.data.customers;

    customerData.forEach((customer)=>{
      let default_address = customer.default_address;

      if (default_address){
        let customerId = customer.id;
        let email = customer.email;
        let accepts_marketing = customer.accepts_marketing;
        let created_at = customer.created_at;
        let updated_at = customer.updated_at;
        let first_name = customer.first_name;
        let last_name = customer.last_name;
        let orders_count = customer.orders_count;
        let total_spent = customer.total_spent;
        let last_order_id = customer.last_order_id;
        let last_order_name = customer.last_order_name;
        let address1 = default_address.address1;
        let address2 = default_address.address2;
        let city = default_address.city;
        let province = default_address.province;
        let province_code = default_address.province_code;
        let country = default_address.country;
        let zip = default_address.zip;
        let phone = default_address.phone;

        //determine if subscriber is active
        let tags = customer.tags;
        let activeSub = false;

        //if sub is active set active boolean
        if(tags === 'active_subscriber'){
          activeSub = true;
        }

        let checkCustomerDb = AllCustomers.findOne({customerId:customerId});

        if (checkCustomerDb === undefined || null) {
          let newCustomer = AllCustomers.insert({
            customerId:customerId,
            email:email,
            accepts_marketing:accepts_marketing,
            created_at:created_at,
            updated_at:updated_at,
            first_name:first_name,
            last_name:last_name,
            orders_count:orders_count,
            total_spent:total_spent,
            last_order_id:last_order_id,
            active:activeSub,
            last_order_name:last_order_name,
            address1:address1,
            address2:address2,
            city:city,
            province:province,
            province_code:province_code,
            country:country,
            zip:zip,
            phone:phone,
            systemCreatedOn: new Date()
          });

        }
      } else {
        let customerId = customer.id;
        let email = customer.email;
        let accepts_marketing = customer.accepts_marketing;
        let created_at = customer.created_at;
        let updated_at = customer.updated_at;
        let first_name = customer.first_name;
        let last_name = customer.last_name;
        let orders_count = customer.orders_count;
        let total_spent = customer.total_spent;
        let last_order_id = customer.last_order_id;
        let last_order_name = customer.last_order_name;

        //determine if subscriber is active
        let tags = customer.tags;
        let activeSub = false;

        //if sub is active set active boolean
        if(tags === 'active_subscriber'){
          activeSub = true;
        }

        let checkCustomerDb = AllCustomers.findOne({customerId:customerId});

        if (checkCustomerDb === undefined || null) {
          let newCustomer = AllCustomers.insert({
            customerId:customerId,
            email:email,
            accepts_marketing:accepts_marketing,
            created_at:created_at,
            updated_at:updated_at,
            first_name:first_name,
            last_name:last_name,
            orders_count:orders_count,
            total_spent:total_spent,
            last_order_id:last_order_id,
            active:activeSub,
            last_order_name:last_order_name,
            systemCreatedOn: new Date()
          });

        }
      }
    });
  }

});