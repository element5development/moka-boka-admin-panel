Template.NewUsers.onRendered(function(){
  $('select').material_select();
});

Template.NewUsers.events({
  'submit #signUpForm'(event){
    event.preventDefault();
    var target = event.target;

    const getFirst = target.signUpFirstName.value;
    const getLast = target.signUpLastName.value;
    const getEmail = target.signUpEmail.value;
    const getPhone = target.signUpTel.value;
    const getPass = target.signUpPass.value;
    const getAccount = target.signUpAccount.value;

    Meteor.call('addSystemUsers',getFirst,getLast,getEmail,getPhone,getPass,getAccount);

    target.signUpFirstName.value = "";
    target.signUpLastName.value = "";
    target.signUpEmail.value = "";
    target.signUpTel.value = "";
    target.signUpPass.value = "";
    target.signUpAccount.value = "";

    sAlert.success(getFirst + " " + getLast + " "+ "successfully added!");
  }
});