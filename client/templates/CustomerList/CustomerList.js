import {AllCustomers} from "../../../imports/api/Customers/AllCustomers/AllCustomers";

Template.CustomerList.onCreated(function () {
  Meteor.call("getAllCustomers");

  this.autorun(() =>{
    this.subscribe("publishAllCustomers");
  });
});

Template.CustomerList.helpers({
  getCustomers: ()=>{
    return AllCustomers.find({}, {sort:{active:-1,last_name:-1}});
  }
});