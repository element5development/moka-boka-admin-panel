import { AllOrders } from '../../../imports/api/Orders/AllOrders/AllOrders';
import { AllAttributes } from '../../../imports/api/Orders/OrderAttributes/OrderAttributes';
import { AllLineItems } from '../../../imports/api/Orders/LineItems/LineItems';

Template.OrderList.onCreated(function () {
  Meteor.call("getAllOrders");

  this.autorun(() =>{
    this.subscribe("publishAllOrders");
    this.subscribe("publishAllAttributes");
    this.subscribe("publishAllLineItems");
  });
});

Template.OrderList.helpers({
  getOrders: ()=>{
    return AllOrders.find({}, {sort:{refund:1,realOrderId:-1}});
  }
});