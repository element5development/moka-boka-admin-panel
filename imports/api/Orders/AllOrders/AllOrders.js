import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

export const AllOrders = new Mongo.Collection('orders');

OrderSchema = new SimpleSchema({
  _id: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    optional: true
  },

  realOrderId: {
    type: Number,
    optional: true
  },

  orderId: {
    type: Number,
    optional: true
  },

  refund: {
    type: Boolean,
    optional: true
  },

  hasAttr: {
    type: Boolean,
    optional: true
  },

  AttrId: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    optional: true
  },

  orderDate: {
    type: Date,
    optional: true
  },

  customerId: {
    type: Number,
    optional: true
  },

  customerFirstName: {
    type: String,
    optional: true
  },

  customerLastName: {
    type: String,
    optional: true
  },

  customerEmail: {
    type: String,
    optional: true
  },

  printStatus: {
    type: Boolean,
    optional: true
  },

  createdOn: {
    type: Date,
    optional: true
  }
});

AllOrders.attachSchema( OrderSchema );