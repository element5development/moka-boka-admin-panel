Template.Header.onRendered(function () {
  $(".button-collapse").sideNav();
});

Template.Header.events({
  'click .LogOut': function(event){
    event.preventDefault();

    Meteor.logout(function(error) {
      if(error) {
        console.log("ERROR: " + error.reason);
      }
      sAlert.error("Successfully logged out");
    });
  },
});