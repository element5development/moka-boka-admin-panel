import { AllOrders } from '../../../imports/api/Orders/AllOrders/AllOrders';
import { AllAttributes } from '../../../imports/api/Orders/OrderAttributes/OrderAttributes';
import { AllLineItems } from '../../../imports/api/Orders/LineItems/LineItems';


Template.OrderDetails.onCreated(function () {
  let thisOrderId = FlowRouter.getParam('id');

  this.autorun(() =>{
    this.subscribe("singleOrder", thisOrderId);
    this.subscribe("singleAttributes", thisOrderId);
    this.subscribe("orderLineItems", thisOrderId);
  });
});

Template.OrderDetails.helpers({
  OrderInfo: function () {
    let id = FlowRouter.getParam('id');
    return AllOrders.findOne({_id:id});
  },
  OrderAtts: function () {
    let id = FlowRouter.getParam('id');
    return AllAttributes.findOne({orderId:id});
  },
  LineItems: function () {
    let id = FlowRouter.getParam('id');
    return AllLineItems.find({orderId:id});
  }
});

Template.OrderDetails.events({
  "click #downloadButton":function(){
    let id = FlowRouter.getParam('id');
    let customerInfo = AllOrders.find({_id:id}, {fields: {_id:0, customerEmail:1, customerLastName:1, customerFirstName:1, orderId:1}}).fetch();
    let attrInfo = AllAttributes.find({orderId:id}, {fields: {_id:0, createdAt: 0, orderId:0}}).fetch();
    let combinedObjects = Object.assign(customerInfo[0], attrInfo[0]);
    let makeArray = new Array(combinedObjects);
    console.log(makeArray);

    var csvContent = CSV.unparse(makeArray);
    window.open('data:text/csv;charset=utf-8,' + escape(csvContent), '_self');
  }
});