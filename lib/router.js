/////Public Routes/////
//////////////////////
//logout redirect
Accounts.onLogout(function(){
  FlowRouter.go('login');
});

//Public Group
const exposed = FlowRouter.group({});

//Main Login Page
exposed.route('/', {
  name: 'login',
  action() {
    BlazeLayout.render('FullWithOutHeader', {main: 'LogIn'});
  }
});


/////Secure Routes/////
//////////////////////

//Logged In Route Group
const loggedIn = FlowRouter.group({});

//Register
loggedIn.route('/register', {
  name: 'new-users',
  action() {
    BlazeLayout.render('FullWithHeader', {main: 'NewUsers'});
  }
});

//Dashboard
loggedIn.route('/dashboard', {
  name: 'dashboard',
  action() {
    BlazeLayout.render('FullWithHeader', {main: 'Dashboard'});
  }
});

///////////Order Specific Routes//////////////
/////////////////////////////////////////////

//Order List
loggedIn.route('/order-list', {
  name: 'order-list',
  action() {
    BlazeLayout.render('FullWithHeader', {main: 'OrderList'});
  }
});

//Order Details
loggedIn.route('/order/:id', {
  name: 'order-details',
  action() {
    BlazeLayout.render('FullWithHeader', {main: 'OrderDetails'});
  }
});

///////////Customer Specific Routes//////////////
/////////////////////////////////////////////

//Customer List
loggedIn.route('/customer-list', {
  name: 'customer-list',
  action() {
    BlazeLayout.render('FullWithHeader', {main: 'CustomerList'});
  }
});