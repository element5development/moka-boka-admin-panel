# Moka Boka Administrative Panel #
## Installing Locally ##

* Be sure you have Meteor installed either globally or in the directory you intend to install the app. More details on that can be found here: https://www.meteor.com/install
* Clone this repo to your meteor directory
* From your terminal in the app directory run: 
```
#!

meteor npm install
```

* From your terminal run:
```
#!

meteor
```
* This should start the app at: http://localhost:3000/