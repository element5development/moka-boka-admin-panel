Template.LogIn.events({
  'submit #loginForm'(event){
    event.preventDefault();
    var target = event.target;

    const emailVar = target.UserName.value;
    const passVar = target.UserPass.value;

    Meteor.loginWithPassword(emailVar, passVar);
    FlowRouter.go('dashboard');
  }
});