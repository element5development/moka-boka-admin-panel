import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

export const AllLineItems = new Mongo.Collection('lineitems');

LineItemSchema = new SimpleSchema({
  _id: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    optional: true
  },

  lineItemId: {
    type: Number,
    optional: true
  },

  orderId: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    optional: true
  },

  title: {
    type: String,
    optional: true
  },

  quantity: {
    type: Number,
    optional: true
  },

  price: {
    type: String,
    optional: true
  },

  variant_title: {
    type: String,
    optional: true
  },

  product_id: {
    type: Number,
    optional: true
  },

  name: {
    type: String,
    optional: true
  },

  customImage: {
    type: String,
    optional: true
  },

  recurring: {
    type: Boolean,
    optional: true
  },

  recurringTime: {
    type: String,
    optional: true
  },

  createdOn: {
    type: Date,
    optional: true
  }

});

AllLineItems.attachSchema( LineItemSchema );