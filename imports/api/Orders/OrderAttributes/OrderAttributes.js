import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

export const AllAttributes = new Mongo.Collection('orderattributes');

AttributeSchema = new SimpleSchema({

  _id: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    optional: true
  },

  orderId: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    optional: true
  },

  createdAt: {
    type: Date,
    optional: true
  },

  child1FirstName: {
    type: String,
    optional: true,
  },

  child1Gender: {
    type: String,
    optional: true
  },

  child1Birthday: {
    type: String,
    optional: true
  },

  child1HairColor: {
    type: String,
    optional: true
  },

  child1HairStyle: {
    type: String,
    optional: true
  },

  child1SkinTone: {
    type: String,
    optional: true
  },

  child1FavoriteColor: {
    type: String,
    optional: true
  },

  child2FirstName: {
    type: String,
    optional: true
  },

  child2Gender: {
    type: String,
    optional: true
  },

  child2Birthday: {
    type: String,
    optional: true
  },

  child2HairColor: {
    type: String,
    optional: true
  },

  child2HairStyle: {
    type: String,
    optional: true
  },

  child2SkinTone: {
    type: String,
    optional: true
  },

  child2FavoriteColor: {
    type: String,
    optional: true
  },

  relationship1to2: {
    type: String,
    optional: true
  },

  relationship2to1: {
    type: String,
    optional: true
  },

  dedication: {
    type: String,
    optional: true
  },

  noteAtrImage: {
    type: String,
    optional: true
  }

});

AllAttributes.attachSchema( AttributeSchema );